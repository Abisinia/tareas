#!/bin/bash
CONTADOR=0
read -p "¿Cuántos usuarios deseas agregar? " total
while [ $CONTADOR -lt $total ]
do
	echo "Ingresar nombre de usuario: "
	read NAME
	PW=$(cat /dev/urandom | tr -dc [:alpha:] | head -c 15)
	useradd --password $PW --create-home --home-dir=/home/$NAME --shell=/usr/sbin/nologin --gid users $NAME
	chage -M 99 $NAME
	chage -W 5 $NAME
	echo $NAME	$PW >> nadaImportante
	let CONTADOR=CONTADOR+1
done 2>errores.log
